import React, { useState} from 'react'
import './Navbar.css';
import { Link } from 'react-scroll';
import * as GiIcons from 'react-icons/gi'
import * as FaIcons from 'react-icons/fa'
import 'bootstrap/dist/css/bootstrap.min.css';




const Navigation = () => {

    let [open, setOpen] = useState(true);
    

    let name = '<Developer />'

    const isOpen = () => {
        setOpen(!open);
    }

    return (
       
        <nav className="navbar fixed-top navbar-expand-xxl" id='navbar'>
            <h3 className='name anim-typewriter'>{name}</h3>
            <button className="btn navbar-toggler p-2 m-2 rounded-circle ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" onClick={isOpen} aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" id="btn">
			<span className="navbar-toggler-icon">{open ? <GiIcons.GiHamburgerMenu className="toggle" /> : <FaIcons.FaTimes className="toggle"/>} </span>
			</button>

            <div className="collapse navbar-collapse mx-auto" id="navbarSupportedContent" >
            
            <ul className="navbar-nav mr-auto text-center">
                        <Link
                            onClick={isOpen}
                            data-toggle="collapse" 
                            data-target=".navbar-collapse"
                            activeClass="active"
                            to="Home"
                            spy={true}
                            smooth={true}
                            offset={-100}
                            duration={1000}
                            className="links"
                            type='submit'
                            >Home
                        </Link>

                        <Link
                            onClick={isOpen}
                            data-toggle="collapse" 
                            data-target=".navbar-collapse"
                            activeClass="active"
                            to="Project"
                            spy={true}
                            smooth={true}
                            offset={-80}
                            duration={1000}
                            className="links"
                            >Projects
                        </Link>

                          <Link 
                            onClick={isOpen}
                            data-toggle="collapse" 
                            data-target=".navbar-collapse"
                            activeClass="active"
                            to="Skills"
                            spy={true}
                            smooth={true}
                            offset={-70}
                            duration={1000}
                            className="links"
                            >Skills
                          </Link>
  
                          <Link
                            onClick={isOpen}
                            data-toggle="collapse" 
                            data-target=".navbar-collapse"
                            activeClass="active"
                            to="Contact"
                            spy={true}
                            smooth={true}
                            offset={300}
                            duration={1000}
                            className="links"
                         >Contact</Link>

            </ul>
            
            </div>
        </nav>
       


    )
}

export default Navigation