import React from 'react'
import './Footer.css'


const Footer = () => {
    return(
        <>
            <div className='footer-nav'>
                <span className='footer-text'>Prepared by Brylle Rallos</span>
            </div>
        </>
    )
}

export default Footer
