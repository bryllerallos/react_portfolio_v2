import React, {useEffect} from 'react';
import './Skills.css';
import HTML from '../../images/html.png';
import CSS from '../../images/css.png';
import JS from '../../images/js.png';
import Bootstrap from '../../images/bootstrap.png';
import PHP from '../../images/php.png';
import MYSQL from '../../images/mysql.png';
import Laravel from '../../images/laravel.png';
import Mongo from '../../images/mongodb.png';
import Express from '../../images/express.png';
import Node from '../../images/nodejs.png';
import Postman from '../../images/postman.png';
import ReactJs from '../../images/react.png';
import Gitlab from '../../images/gitlab.png';
import Next from '../../images/nextjs.png';
import Aos from 'aos';
import 'aos/dist/aos.css';


const Skills = () => {

    useEffect(() => {
    Aos.init({ duration: 3000 });
  }, []);


    return(
        <>
        <div className="container my-5" id="Skills">
            <h1 className="title my-5 text-center" data-aos="zoom-in">Skills</h1>
            <div className="col-lg-6 text-center"id="col" >
                <img src={HTML} className="skillImg m-3" alt="HTML" title="HTML" id="html" data-aos="fade-up"
                data-aos-easing="linear" />
               
                <img src={CSS} className="skillImg m-3" alt="CSS" title="CSS" id="css" data-aos="fade-down" data-aos-easing="linear"/>
                <img src={JS} className="skillImg m-3" alt="Javascript" title="Javascript" id="js" data-aos="fade-up"
                data-aos-easing="linear"/>
                <img src={Bootstrap} className="skillImg m-3" alt="Bootstrap" title="Bootstrap" id="bootstrap" data-aos="fade-down" data-aos-easing="linear"/>
                <img src={PHP} id="php" className="skillImg m-2" alt="PHP" title="PHP" data-aos="fade-up"
                data-aos-easing="linear"/>
                <img src={MYSQL} className="skillImg m-3" alt="MYSQL" title="MYSQL" data-aos="fade-down" data-aos-easing="linear" id="sql"/>
                <img src={Laravel} className="skillImg m-3" alt="Laravel" title="Laravel" id="laravel" data-aos="fade-up"
                data-aos-easing="linear"/>
                <img src={Mongo} className="skillImg m-3" alt="MongoDb" title="MongoDb" id="mongo" data-aos="fade-down" data-aos-easing="linear"/>
                <img src={Express} className="skillImg m-3" alt="ExpressJs" title="ExpressJs" id="express" data-aos="fade-up"
                data-aos-easing="linear"/>
                <img src={Node} className="skillImg m-3" alt="NodeJs" title="NodeJs" id="node" data-aos="fade-down" data-aos-easing="linear"/>
                <img src={Postman} id="postman" className="skillImg m-3" alt="Postman" title="Postman" data-aos="fade-up"
                 data-aos-easing="linear"/>
                <img src={ReactJs} className="skillImg m-3" alt="ReactJs" title="ReactJs" data-aos="fade-down" data-aos-easing="linear" id="react"/>
                <img src={Gitlab} className="skillImg m-3" alt="Gitlab" title="Gitlab" data-aos="fade-up"
                 data-aos-easing="linear" id="gitlab"/>
                 <img src={Next} className="skillImg m-3" alt="Next" title="Next" data-aos="fade-up"
                 data-aos-easing="linear" id="gitlab"/>
            </div>
        </div>
        </>
    )
}

export default Skills